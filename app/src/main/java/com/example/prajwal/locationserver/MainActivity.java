package com.example.prajwal.locationserver;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView latitudeView = (TextView) findViewById(R.id.latitudeView);
        final TextView longitudeView = (TextView) findViewById(R.id.longitudeView);
        final TextView accuracyView = (TextView) findViewById(R.id.accuracyView);
        final TextView timeView = (TextView) findViewById(R.id.timeView);
        final TextView altitudeView = (TextView) findViewById(R.id.altitudeView);
        final TextView speedView = (TextView) findViewById(R.id.speedView);
        final TextView bearingView = (TextView) findViewById(R.id.bearingView);
        final TextView satellitesView = (TextView) findViewById(R.id.satellitesView);
        final EditText postUrl = (EditText) findViewById(R.id.postUrl);
        final Switch toggleSending = (Switch) findViewById(R.id.toggleSending);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        String locationProvider = LocationManager.GPS_PROVIDER;

        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                String latitude = Double.toString(location.getLatitude());
                String longitude = Double.toString(location.getLongitude());
                String accuracy = Float.toString(location.getAccuracy());
                String time = Long.toString(location.getTime());
                String altitude = Double.toString(location.getAltitude());
                String speed = Double.toString(location.getSpeed() * 3.6);
                String bearing = Float.toString(location.getBearing());
                String satellites = Integer.toString(location.getExtras().getInt("satellites"));

                latitudeView.setText(latitude);
                longitudeView.setText(longitude);
                accuracyView.setText(accuracy);
                timeView.setText(time);
                altitudeView.setText(altitude);
                speedView.setText(speed + " km/hr");
                bearingView.setText(bearing);
                satellitesView.setText(satellites);

                if (toggleSending.isChecked()) {

                    String urlString = postUrl.getText().toString();

                    urlString = "http://" + urlString + "/?" +
                            "latitude=" + latitude +
                            "&longitude=" + longitude +
                            "&accuracy=" + accuracy +
                            "&time=" + time +
                            "&altitude=" + altitude +
                            "&speed=" + speed +
                            "&bearing=" + bearing +
                            "&satellites=" + satellites;

                    Log.v("Post URL", urlString);

                    try {
                        URL url = new URL(urlString);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(10000);
                        conn.setConnectTimeout(15000);
                        conn.setChunkedStreamingMode(0);
                        conn.setRequestMethod("GET");
                        conn.setDoInput(true);

                        conn.connect();
                        int response = conn.getResponseCode();
                        Log.v("Response: ", Integer.toString(response));

                    } catch (ProtocolException e) {
                        Context context = getApplicationContext();
                        CharSequence text = "Protocol Exception:" + e;
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        e.printStackTrace();

                    } catch (MalformedURLException e) {
                        Context context = getApplicationContext();
                        CharSequence text = "Malformed URL Exception:" + e;
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        e.printStackTrace();

                    } catch (IOException e) {
                        Context context = getApplicationContext();
                        CharSequence text = "IO Exception:" + e;
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(locationProvider, 3000, 0, locationListener);
    }
}
